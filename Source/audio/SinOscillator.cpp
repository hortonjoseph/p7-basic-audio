//
//  SinOscillator.cpp
//  JuceBasicAudio
//
//  Created by Joseph Horton on 13/11/2017.
//
//

#include "SinOscillator.hpp"

SinOscillator::SinOscillator()
{
    sampleRate = 44100;

}

SinOscillator::~SinOscillator()
{
    
}

float SinOscillator::getSample()
{
    float sample = sin(phasePosition);
    phasePosition += phaseIncrement;
    if (phasePosition > 2.f * M_PI)
    {
        phasePosition -= 2.f * M_PI;
    }
    
    return sample;
}

void SinOscillator::setFrequency(float _frequency)
{
    frequency = _frequency;
    
    phaseIncrement = ((2.f * M_PI) * frequency )/sampleRate;

}

void SinOscillator::setAmplitude(float _amplitude)
{
    amplitude = _amplitude;
}

void SinOscillator::setSampleRate (float _sampleRate)
{
    sampleRate = _sampleRate;
    setFrequency (frequency);
}
