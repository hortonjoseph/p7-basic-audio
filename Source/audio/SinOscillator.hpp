//
//  SinOscillator.hpp
//  JuceBasicAudio
//
//  Created by Joseph Horton on 13/11/2017.
//
//

#ifndef SinOscillator_hpp
#define SinOscillator_hpp

#include <stdio.h>
#include <math.h>

class SinOscillator
{
public:
    SinOscillator();
    
    ~SinOscillator();
    
    float getSample();
    
    void setFrequency(float _frequency);
    
    void setAmplitude(float _amplitude);
    
    void setSampleRate(float _sampleRate);
private:
    
    float frequency;
    float phasePosition;
    float sampleRate;
    float amplitude;
    float phaseIncrement;
};


#endif /* SinOscillator_hpp */

